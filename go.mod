module gitlab.com/wobcom/chirpstack_devices_exporter

go 1.15

require (
	git.xsfx.dev/xsteadfastx/logginghandler v0.0.1
	github.com/prometheus/client_golang v1.7.1
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
)
