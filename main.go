// nolint: gochecknoglobals, gomnd, exhaustivestruct, goerr113
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"git.xsfx.dev/xsteadfastx/logginghandler"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type chirpi struct {
	URL   string `yaml:"url"`
	Token string `yaml:"token"`
	Port  int    `yaml:"port"`
}

var (
	timeout = 15 * time.Second
	version = "dev"
	commit  = "none"
	date    = "unknown"
)
var configFile string

var rootCmd = &cobra.Command{
	Use:   "chirpstack-devices-exporter",
	Short: "Chirpstack Prometheus Exporter",
	Run: func(cmd *cobra.Command, args []string) {
		if err := cmd.Help(); err != nil {
			log.Fatal().Msg(err.Error())
		}
		os.Exit(0)
	},
}

var Metrics = map[string]*prometheus.Desc{
	"devices": prometheus.NewDesc(
		prometheus.BuildFQName("chirpstack", "devices", "last_seen"),
		"timestamp when devices was last seen",
		[]string{
			"app",
			"app_id",
			"dev_eui",
			"name",
			"device_profile",
			"organization_id",
			"productive",
		},
		nil,
	),
	"battery": prometheus.NewDesc(
		prometheus.BuildFQName("chirpstack", "devices", "battery_status"),
		"battery status of devices",
		[]string{
			"app",
			"app_id",
			"dev_eui",
			"name",
			"device_profile",
			"organization_id",
			"productive",
		},
		nil,
	),
	"devices_not_seen": prometheus.NewDesc(
		prometheus.BuildFQName("chirpstack", "devices", "not_seen"),
		"devices which are not seen in chirpstack",
		nil,
		nil,
	),
	"gateways": prometheus.NewDesc(
		prometheus.BuildFQName("chirpstack", "gateways", "last_seen"),
		"gateways timestamp last seen in chirpstack",
		[]string{
			"id",
			"name",
			"organization_id",
			"productive",
		},
		nil,
	),
	"gateways_not_seen": prometheus.NewDesc(
		prometheus.BuildFQName("chirpstack", "gateways", "not_seen"),
		"gateways which are not seen in chirpstack",
		nil,
		nil,
	),
	"up": prometheus.NewDesc(
		prometheus.BuildFQName("chirpstack", "exporter", "up"),
		"exporter status",
		nil,
		nil,
	),
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run the exporter",
	Run: func(cmd *cobra.Command, args []string) {
		if configFile != "" {
			viper.SetConfigFile(configFile)

			if err := viper.ReadInConfig(); err != nil {
				log.Fatal().Msg(err.Error())
			}
		}

		for _, i := range []string{"url", "token"} {
			if !viper.IsSet(i) {
				log.Fatal().Msgf("missing \"%s\"", i)
			}
		}

		c := chirpi{
			URL:   viper.GetString("url"),
			Token: viper.GetString("token"),
			Port:  viper.GetInt("port"),
		}

		prometheus.MustRegister(c)

		mux := http.NewServeMux()

		mux.HandleFunc(
			"/healthz",
			func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
			},
		)

		mux.Handle("/metrics", promhttp.Handler())

		log.Info().Int("port", c.Port).Msg("starting...")
		log.Fatal().Msg(http.ListenAndServe(fmt.Sprintf(":%d", c.Port), logginghandler.Handler(mux)).Error())
	},
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("chirpstack-devices-exporter %s, commit %s, built at %s", version, commit, date)
	},
}

type (
	Device struct {
		App            string
		AppID          string
		DevEUI         string
		Name           string
		DeviceProfile  string
		OrganizationID string
		LastSeenAt     time.Time
		BatteryLevel   int
		Productive     string
	}

	Devices []Device

	Gateway struct {
		ID             string
		Name           string
		LastSeenAt     time.Time
		OrganizationID string
		Productive     string
	}

	Gateways []Gateway
)

func (c chirpi) Describe(ch chan<- *prometheus.Desc) {
	for _, v := range Metrics {
		ch <- v
	}
}

// nolint: funlen
func (c chirpi) Collect(ch chan<- prometheus.Metric) {
	// Gateways.
	gtws, err := c.getGateways()
	if err != nil {
		ch <- prometheus.MustNewConstMetric(Metrics["up"], prometheus.GaugeValue, 0)

		return
	}

	gwsNotSeenCount := float64(0)

	for _, g := range gtws {
		if g.LastSeenAt.IsZero() {
			gwsNotSeenCount++

			continue
		}

		metric, err := prometheus.NewConstMetric(
			Metrics["gateways"],
			prometheus.GaugeValue,
			float64(g.LastSeenAt.Unix()),
			g.ID,
			g.Name,
			g.OrganizationID,
			g.Productive,
		)
		if err != nil {
			ch <- prometheus.MustNewConstMetric(Metrics["up"], prometheus.GaugeValue, 0)

			return
		}
		ch <- metric
	}

	ch <- prometheus.MustNewConstMetric(Metrics["gateways_not_seen"], prometheus.GaugeValue, gwsNotSeenCount)

	// Devices.
	dvsNotSeenCount := float64(0)

	dvs, err := c.getDevices()
	if err != nil {
		log.Error().Msg(err.Error())
		ch <- prometheus.MustNewConstMetric(Metrics["up"], prometheus.GaugeValue, 0)

		return
	}

	for _, d := range dvs {
		if d.LastSeenAt.IsZero() {
			dvsNotSeenCount++

			continue
		}

		ch <- prometheus.MustNewConstMetric(
			Metrics["devices"],
			prometheus.GaugeValue,
			float64(d.LastSeenAt.Unix()),
			d.App,
			d.AppID,
			d.DevEUI,
			d.Name,
			d.DeviceProfile,
			d.OrganizationID,
			d.Productive,
		)

		// Metrics only for devices that have a non default battery level.
		if d.BatteryLevel != 256 {
			ch <- prometheus.MustNewConstMetric(
				Metrics["battery"],
				prometheus.GaugeValue,
				float64(d.BatteryLevel),
				d.App,
				d.AppID,
				d.DevEUI,
				d.Name,
				d.DeviceProfile,
				d.OrganizationID,
				d.Productive,
			)
		}
	}

	ch <- prometheus.MustNewConstMetric(Metrics["devices_not_seen"], prometheus.GaugeValue, dvsNotSeenCount)

	// Set overall status metric.
	ch <- prometheus.MustNewConstMetric(Metrics["up"], prometheus.GaugeValue, 1)
}

func (c *chirpi) GETRequest(endpoint string) ([]byte, error) {
	fullURL := c.URL + endpoint
	logger := log.With().Str("url", fullURL).Logger()

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	req, err := http.NewRequest("GET", fullURL, nil)
	if err != nil {
		logger.Error().Msg(err.Error())

		return []byte{}, fmt.Errorf("%w", err)
	}

	req.Header.Set("Grpc-Metadata-Authorization", c.Token)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	cli := &http.Client{}

	logger.Debug().Msg("doing the request")

	resp, err := cli.Do(req.WithContext(ctx))
	if err != nil {
		logger.Error().Msg(err.Error())

		return []byte{}, fmt.Errorf("%w", err)
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.Error().Int("status_code", resp.StatusCode).Msg(err.Error())

		return []byte{}, fmt.Errorf("%w", err)
	}

	logger.Debug().Int("status_code", resp.StatusCode).Msg("got response")

	if resp.StatusCode != http.StatusOK {
		return []byte{}, fmt.Errorf("%s", string(data))
	}

	return data, nil
}

type APIApps struct {
	Result []struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"result"`
}

type APIDevices struct {
	Result []struct {
		DevEUI string `json:"devEUI"`
	} `json:"result"`
}

type APIDevice struct {
	Device struct {
		DevEUI          string            `json:"devEUI"`
		Name            string            `json:"name"`
		DeviceProfileID string            `json:"deviceProfileID"`
		Tags            map[string]string `json:"tags"`
	} `json:"device"`
	LastSeenAt          time.Time `json:"lastSeenAt"`
	DeviceStatusBattery int       `json:"deviceStatusBattery"`
	DeviceStatusMargin  int       `json:"deviceStatusMargin"`
}

type APIDeviceProfile struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	OrganizationID string `json:"organizationID"`
}

type APIDeviceProfiles struct {
	Result []APIDeviceProfile `json:"Result"`
}

func (c *chirpi) apiApps() (APIApps, error) {
	apps := APIApps{}

	data, err := c.GETRequest("/applications?limit=500")
	if err != nil {
		return apps, fmt.Errorf("%w", err)
	}

	if err := json.Unmarshal(data, &apps); err != nil {
		return apps, fmt.Errorf("%w", err)
	}

	return apps, nil
}

func (c *chirpi) apiDeviceProfiles() (map[string]APIDeviceProfile, error) {
	profiles := map[string]APIDeviceProfile{}

	apiProfiles := APIDeviceProfiles{}

	data, err := c.GETRequest("/device-profiles?limit=500")
	if err != nil {
		return profiles, fmt.Errorf("%w", err)
	}

	if err := json.Unmarshal(data, &apiProfiles); err != nil {
		return profiles, fmt.Errorf("%w", err)
	}

	for _, i := range apiProfiles.Result {
		profiles[i.ID] = i
	}

	return profiles, nil
}

func (c *chirpi) apiDeviceList(appID string) (APIDevices, error) {
	devices := APIDevices{}

	data, err := c.GETRequest(fmt.Sprintf("/devices?applicationID=%s&limit=500", appID))
	if err != nil {
		return devices, fmt.Errorf("%w", err)
	}

	if err := json.Unmarshal(data, &devices); err != nil {
		return devices, fmt.Errorf("%w", err)
	}

	return devices, nil
}

// nolint: funlen
func (c *chirpi) getDevices() (Devices, error) {
	dvs := Devices{}

	// Apps.
	apps, err := c.apiApps()
	if err != nil {
		return dvs, fmt.Errorf("%w", err)
	}

	// Device Profiles.
	profiles, err := c.apiDeviceProfiles()
	if err != nil {
		return dvs, fmt.Errorf("%w", err)
	}

	// Devices.
	for _, a := range apps.Result {
		devices, err := c.apiDeviceList(a.ID)
		if err != nil {
			return dvs, fmt.Errorf("%w", err)
		}

		devResp := [][]byte{}
		chURL, chBody, chErr := c.startWorker()

		go func() {
			for _, d := range devices.Result {
				chURL <- "/devices/" + d.DevEUI
			}

			close(chURL)
		}()

		for i := 0; i < len(devices.Result); i++ {
			select {
			case b := <-chBody:
				devResp = append(devResp, b)
			case err := <-chErr:
				return dvs, fmt.Errorf("%w", err)
			}
		}

		for _, d := range devResp {
			device := APIDevice{}

			if err := json.Unmarshal(d, &device); err != nil {
				return dvs, fmt.Errorf("%w", err)
			}

			var prod string

			productive, ok := device.Device.Tags["productive"]
			if ok && productive == "false" { // nolint: goconst
				prod = "false"
			} else {
				prod = "true"
			}

			dvs = append(
				dvs,
				Device{
					App:            a.Name,
					AppID:          a.ID,
					DevEUI:         device.Device.DevEUI,
					Name:           device.Device.Name,
					DeviceProfile:  profiles[device.Device.DeviceProfileID].Name,
					OrganizationID: profiles[device.Device.DeviceProfileID].OrganizationID,
					LastSeenAt:     device.LastSeenAt,
					BatteryLevel:   device.DeviceStatusBattery,
					Productive:     prod,
				},
			)
		}
	}

	return dvs, nil
}

func (c *chirpi) startWorker() (chan<- string, <-chan []byte, <-chan error) {
	chURL := make(chan string)
	chErr := make(chan error)
	chBody := make(chan []byte)

	// start the worker in goroutines
	for i := 0; i < 16; i++ {
		go func(chURL <-chan string, chBody chan<- []byte, chErr chan<- error) {
			for u := range chURL {
				b, err := c.GETRequest(u)
				if err != nil {
					chErr <- err

					continue
				}

				chBody <- b
			}
		}(chURL, chBody, chErr)
	}

	return chURL, chBody, chErr
}

// nolint: funlen
func (c *chirpi) getGateways() (Gateways, error) {
	type APIGateways struct {
		Result []struct {
			ID string `json:"id"`
		} `json:"result"`
	}

	type APIGateway struct {
		Gateway struct {
			ID             string            `json:"id"`
			Name           string            `json:"name"`
			OrganizationID string            `json:"organizationID"`
			Tags           map[string]string `json:"tags"`
		} `json:"gateway"`
		LastSeenAt time.Time `json:"lastSeenAt"`
	}

	gateways := Gateways{}

	gtws := &APIGateways{}

	data, err := c.GETRequest("/gateways?limit=500")
	if err != nil {
		return gateways, err
	}

	if err := json.Unmarshal(data, gtws); err != nil {
		return gateways, fmt.Errorf("%w", err)
	}

	gwResp := [][]byte{}

	chURL, chBody, chErr := c.startWorker()

	go func() {
		for _, i := range gtws.Result {
			chURL <- "/gateways/" + i.ID
		}

		close(chURL)
	}()

	for i := 0; i < len(gtws.Result); i++ {
		select {
		case b := <-chBody:
			gwResp = append(gwResp, b)
		case err := <-chErr:
			return gateways, fmt.Errorf("%w", err)
		}
	}

	for _, i := range gwResp {
		gw := &APIGateway{}

		if err := json.Unmarshal(i, gw); err != nil {
			return gateways, fmt.Errorf("%w", err)
		}

		var prod string

		productive, ok := gw.Gateway.Tags["productive"]
		if ok && productive == "false" {
			prod = "false"
		} else {
			prod = "true"
		}

		gateways = append(
			gateways,
			Gateway{
				ID:             gw.Gateway.ID,
				Name:           gw.Gateway.Name,
				LastSeenAt:     gw.LastSeenAt,
				OrganizationID: gw.Gateway.OrganizationID,
				Productive:     prod,
			},
		)
	}

	return gateways, nil
}

func main() {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})

	log.Logger = log.With().Caller().Logger()

	// Defaults.
	defaultPort := 2112

	viper.SetEnvPrefix("chirp")
	viper.AutomaticEnv()

	rootCmd.AddCommand(runCmd)
	rootCmd.AddCommand(versionCmd)

	runCmd.Flags().StringVar(&configFile, "config", "", "config file")

	runCmd.Flags().String("token", "", "api token")

	if err := viper.BindPFlag("token", runCmd.Flags().Lookup("token")); err != nil {
		log.Fatal().Msg(err.Error())
	}

	runCmd.Flags().String("url", "", "chirpstack url")

	if err := viper.BindPFlag("url", runCmd.Flags().Lookup("url")); err != nil {
		log.Fatal().Msg(err.Error())
	}

	runCmd.Flags().Int("port", defaultPort, "exporter port")

	if err := viper.BindPFlag("port", runCmd.Flags().Lookup("port")); err != nil {
		log.Fatal().Msg(err.Error())
	}

	if err := rootCmd.Execute(); err != nil {
		log.Fatal().Msg(err.Error())
	}
}
