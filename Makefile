.PHONY: generate build release test lint dep-update

generate:
	GOFLAGS=-mod=vendor go generate ./...

build:
	goreleaser build --rm-dist --snapshot

release:
	goreleaser release --rm-dist --snapshot --skip-publish

test:
	GOFLAGS=-mod=vendor go test -race -cover -v ./...

lint:
	golangci-lint run --enable-all --disable gomnd --disable godox --timeout 5m

dep-update:
	go get -u ./...
	go test ./...
	go mod tidy
	go mod vendor
