# chirpstack-device-exporter

[![wobcom](./wobcom-badge.png)](https://gitlab.com/wobcom)
[![Pipeline Status](https://gitlab.com/wobcom/chirpstack-devices-exporter/badges/master/pipeline.svg)](https://gitlab.com/wobcom/chirpstack-devices-exporter/-/commits/master)

![tony](./README.gif)

A prometheus exporter that gets the `last seen at` data from the [ChirpStack](https://www.chirpstack.io/) API to get metrics about a device maybe offline for too long. Like this you can easily configure a prometheus alertmanager or a create a grafana table.

## Usage

There are several ways to use the exporter. There are Binaries, Distro-Packages and Docker-Images available on the [release page](https://gitlab.com/wobcom/chirpstack-devices-exporter/-/releases).

### Config file

Its possible to set everything up in a config file. Running it with `chirpstack-devices-exporter run --config config.yaml`.

#### Example

        ---
        token: f00b4r
        url: http://chirpstack.foo.tld/api
        port: 2112
        ignore_tags:
          - foo
          - bar

### Commandline flags

#### Example

        chirpstack-devices-exporter run --token f00b4r --url http://chirpstack.foo.tld/api --port 2112 --ignore-tags foo --ignore-tags bar

### Environment variables

#### Example

        CHIRP_TOKEN=f00b4r CHIRP_URL=http://chirpstack.foo.tld/api CHIRP_PORT=2112 chirpstack-devices-exporter

## Metrics

### `chirpstack_devices_last_seen`

Type: Gauge

This metric exports the unix timestamp when a device was last seen. It ignores the ones that never got seen by ChirpStack.

#### Labels

- `app`: Name of the app this device is part of.
- `app_id`: ID of the app this device is part of.
- `dev_eui`: ID of the device itself.
- `name`: Name of the device.
- `device_profile`: Name of the device profile.
- `organization_id`: ID of the chirpstack orgranization.
- `productive`: True or False if a productive tag is set in chirpstack.

### `chirpstack_devices_not_seen`

Type: Gauge

This metric is a simple gauge of the number of devices that was never been seen by ChirpStack.

### `chirpstack_devices_battery_status`

Type: Gauge

This metric exports the battery status of the devices.

#### Labels

- `app`: Name of the app this device is part of.
- `app_id`: ID of the app this device is part of.
- `dev_eui`: ID of the device itself.
- `name`: Name of the device.
- `device_profile`: Name of the device profile.
- `organization_id`: ID of the chirpstack orgranization.
- `productive`: True or False if a productive tag is set in chirpstack.

### `chirpstack_gateways_last_seen`

Type: Gauge

This metric exports the unix timestamp when a gateway was last seen. It ignores the ones that never got seen by ChirpStack.

#### Labels

- `id`: ID of the gateway.
- `name`: Name of the gateway.
- `orgranization_id`: ID of the chirpstack orgranization
- `productive`: True or False if a productive tag is set in chirpstack.

### `chirpstack_gateways_not_seen`

Type: Gauge

This metric is a simple gauge of the number of gateways that was never been seen by ChirpStack.

### `chirpstack_exporter_up`

Type: Gauge

This metric is `1` when the API scrapes worked out and `0` when something went wrong.
